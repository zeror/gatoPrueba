package com.droidrank.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button block1, block2, block3, block4, block5, block6, block7, block8, block9, restart;
    TextView result;
    LinearLayout lineaB;

    boolean terminar = false;
    public static String jugador = "O";
    public int posicionBoton;

    public static String[][] gato = new String[3][3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        block1 = (Button) findViewById(R.id.bt_block1);
        block2 = (Button) findViewById(R.id.bt_block2);
        block3 = (Button) findViewById(R.id.bt_block3);
        block4 = (Button) findViewById(R.id.bt_block4);
        block5 = (Button) findViewById(R.id.bt_block5);
        block6 = (Button) findViewById(R.id.bt_block6);
        block7 = (Button) findViewById(R.id.bt_block7);
        block8 = (Button) findViewById(R.id.bt_block8);
        block9 = (Button) findViewById(R.id.bt_block9);
        result = (TextView) findViewById(R.id.tv_show_result);
        restart = (Button) findViewById(R.id.bt_restart_game);
        lineaB = (LinearLayout) findViewById(R.id.linearButton);

        init();

        /**
         * Restarts the game
         */
        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                terminar = false;
                jugador = "O";
                clearForm(lineaB);
                gato = new String[3][3];
                inicializarTablero();
            }
        });
        
    // Final de onCreate
    }

    private void clearForm(LinearLayout group) {
        for (int i = 0, count = group.getChildCount(); i < count; ++i) {
            View view = group.getChildAt(i);
            if (view instanceof Button) {
                if (!((Button)view).getText().equals(getString(R.string.restart_button_text_initially))){
                    ((Button)view).setText("");
                }
            }

            if(view instanceof LinearLayout && (((LinearLayout)view).getChildCount() > 0))
                clearForm((LinearLayout)view);
        }
    }

    public void init(){
        block1.setOnClickListener(this);
        block2.setOnClickListener(this);
        block3.setOnClickListener(this);
        block4.setOnClickListener(this);
        block5.setOnClickListener(this);
        block6.setOnClickListener(this);
        block7.setOnClickListener(this);
        block8.setOnClickListener(this);
        block9.setOnClickListener(this);
        inicializarTablero();
    }

    @Override
    public void onClick(View v){
            if (!terminar){
                presionaBoton(v);
                registraEntrada(jugador);
                imprimirGato();
                if (hayGanador(jugador)){
                    Toast.makeText(getApplicationContext(), "Felicidades!", Toast.LENGTH_SHORT).show();
                    terminar = true;
                }else{
                    if (!hayEspacio()){
                        Toast.makeText(getApplicationContext(), "Empate!", Toast.LENGTH_SHORT).show();
                        terminar = true;
                    }else {
                        if (jugador == "O"){
                            jugador = "X";
                        }else{
                            jugador = "O";
                        }
                    }
                }
            }else {
                Toast.makeText(getApplicationContext(), "Termino juego", Toast.LENGTH_SHORT).show();
            }
    }
    
    public void registraEntrada(String caracter){
        boolean salir = false;

        do {
            if (casillaNoOcupada(posicionBoton)){
                switch (posicionBoton){
                    case 1: gato[0][0] = caracter; break;
                    case 2: gato[0][1] = caracter; break;
                    case 3: gato[0][2] = caracter; break;
                    case 4: gato[1][0] = caracter; break;
                    case 5: gato[1][1] = caracter; break;
                    case 6: gato[1][2] = caracter; break;
                    case 7: gato[2][0] = caracter; break;
                    case 8: gato[2][1] = caracter; break;
                    case 9: gato[2][2] = caracter; break;
                }
                salir = true;
            }else{
                Toast.makeText(MainActivity.this, "Casilla no válida, escriba una posición valida", Toast.LENGTH_SHORT).show();
            }
        }while (!salir);
        
    }

    public static boolean casillaNoOcupada(int posicion){
        switch (posicion){
            case 1: return gato[0][0] != " ";
            case 2: return gato[0][1] != " ";
            case 3: return gato[0][2] != " ";
            case 4: return gato[1][0] != " ";
            case 5: return gato[1][1] != " ";
            case 6: return gato[1][2] != " ";
            case 7: return gato[2][0] != " ";
            case 8: return gato[2][1] != " ";
            case 9: return gato[2][2] != " ";
            default: return false;
        }

    }

    public void presionaBoton(View v){
        switch (v.getId()){
            case R.id.bt_block1:
                block1.setText(jugador);
                posicionBoton = 1;
                break;
            case R.id.bt_block2:
                block2.setText(jugador);
                posicionBoton = 2;
                break;
            case R.id.bt_block3:
                block3.setText(jugador);
                posicionBoton = 3;
                break;
            case R.id.bt_block4:
                block4.setText(jugador);
                posicionBoton = 4;
                break;
            case R.id.bt_block5:
                block5.setText(jugador);
                posicionBoton = 5;
                break;
            case R.id.bt_block6:
                block6.setText(jugador);
                posicionBoton = 6;
                break;
            case R.id.bt_block7:
                block7.setText(jugador);
                posicionBoton = 7;
                break;
            case R.id.bt_block8:
                block8.setText(jugador);
                posicionBoton = 8;
                break;
            case R.id.bt_block9:
                block9.setText(jugador);
                posicionBoton = 9;
                break;

        }
    }

    public boolean hayGanador(String jugador){
        if (ganaPorRenglon(jugador)){
            terminar = true;
            return true;
        }
        if (ganaPorColumna(jugador)){
            terminar = true;
            return true;
        }
        return false;
    }

    public boolean ganaPorRenglon(String jugador){
        for (String[] gato1 : gato){
            for (int i = 0; i<gato.length; i++){
                if (gato1[0] == jugador &&
                        gato1[1] == jugador &&
                        gato1[2] == jugador){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean ganaPorColumna(String jugador){
        for (int i=0; i< gato.length; i++){
            if (gato[0][i] == jugador &&
                    gato[1][i] == jugador &&
                    gato[2][i] == jugador){
                return true;
            }
        }
        return false;
    }

    public boolean hayEspacio(){
        for (String[] gato1 : gato){
            for (int i = 0; i< gato.length; i++){
                if (gato1[i] == "-"){
                    return true;
                }
            }
        }
        return false;
    }

    public static void imprimirGato(){
        System.out.println("El gato hasta el momento: ");
        for (String[] gato1 : gato) {
            for (int j = 0; j<gato.length; j++) {
                System.out.print("    " + gato1[j]);
            }
            System.out.println();
        }
    }

    public static void inicializarTablero(){
        for(int i=0;i<gato.length; i++)
            for(int j=0;j<gato.length; j++)
                gato[i][j] = "-";
    }

    // Final de clase
}
